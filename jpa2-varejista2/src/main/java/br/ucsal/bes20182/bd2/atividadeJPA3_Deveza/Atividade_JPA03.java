package br.ucsal.bes20182.bd2.atividadeJPA3_Deveza;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
/*import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
*/

//import org.hibernate.query.NativeQuery;

import br.ucsal.bes20182.bd2.domain.Administrativo;
import br.ucsal.bes20182.bd2.domain.Cidade;
import br.ucsal.bes20182.bd2.domain.Endereco;
import br.ucsal.bes20182.bd2.domain.Estado;
import br.ucsal.bes20182.bd2.domain.Funcionario;
import br.ucsal.bes20182.bd2.domain.PessoaJuridica;
import br.ucsal.bes20182.bd2.domain.RamoAtividade;
import br.ucsal.bes20182.bd2.domain.Vendedor;
import br.ucsal.bes20182.bd2.enums.SituacaoVendedorEnum;

public class Atividade_JPA03 {
	
	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("pu1");
			EntityManager em = emf.createEntityManager();

			popularBase(em);

			em.clear();

			consultaEstadoSemCidade(em);

			consultaFuncionariosAtivos(em, SituacaoVendedorEnum.ATIVO);
			
			consultaClientesPorVendedor(em, "Ruy");
			
			consultaQuantidadeVendedoresCidade(em);
			
			consultaRamosAtividade(em, "Comercial");
			
			consultaVendedoresDaCidade(em, "Camacari");


		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}
	
	private static void popularBase(EntityManager em) {
		em.getTransaction().begin();

		Estado estadoBA = criarEstado(em, "BA", "Bahia");
		Estado estadoSP = criarEstado(em, "SP", "Sao Paulo");
		Estado estadoPE = criarEstado(em, "PE", "Pernambuco");

		Cidade cidade1 = criarCidade(em, estadoBA, "SSA", "Salvador");
		criarCidade(em, estadoBA, "FDS", "Feira de Santana");
		criarCidade(em, estadoBA, "CAM", "Camacari");
		criarCidade(em, estadoSP, "SSP", "Sao Paulo");
		criarCidade(em, estadoSP, "CDJ", "Campos do Jordao");
		
		RamoAtividade ra1 = criarAtividade(em, "Industrial");
		RamoAtividade ra2 = criarAtividade(em, "Comercial");
		RamoAtividade ra3 = criarAtividade(em, "Servicos");
		
		ArrayList<RamoAtividade> ramosAtividades1 = new ArrayList<>();
		ramosAtividades1.add(ra1);
		
		ArrayList<RamoAtividade> ramosAtividades2 = new ArrayList<>();
		ramosAtividades2.add(ra2);
		
		ArrayList<Vendedor> vendedores = new ArrayList<>();
		Vendedor vd1 = new Vendedor();
		vendedores.add(vd1);
		
		PessoaJuridica pj1 = criarPessoaJuridica(em, "01020304050", "Brinquedos GH", 
			ramosAtividades1, 1.5, vendedores);
		PessoaJuridica pj2 = criarPessoaJuridica(em, "06070809010", "Shop Fly", 
				ramosAtividades1, 11.5, vendedores);
		PessoaJuridica pj3 = criarPessoaJuridica(em, "12345678910", "Vestuario Ho", 
				ramosAtividades1, 111.5, vendedores);
		PessoaJuridica pj4 = criarPessoaJuridica(em, "73213532123", "Go CNB", 
				ramosAtividades2, 1111.5, vendedores);
		PessoaJuridica pj5 = criarPessoaJuridica(em, "01561472396", "Go PAIN", 
				ramosAtividades2, 11111.5, vendedores);

		ArrayList<String> telefones = new ArrayList<>();
		telefones.add("719877457445");
		
		Date data = new Date();
		Endereco end = new Endereco("Rua SP", "Pituba", cidade1);
		
		ArrayList<PessoaJuridica> clientes1 = new ArrayList<>();
		clientes1.add(pj1);
		
		ArrayList<PessoaJuridica> clientes2 = new ArrayList<>();
		
		Vendedor vendedor1 = criarVendedor(em, "05442002514", data, end, 
				"Deveza", "6666632322", "SSP", "BA", telefones, 10.5, SituacaoVendedorEnum.ATIVO, clientes1);
		Vendedor vendedor2 = criarVendedor(em, "11111111111", data, end, 
				"Hud", "1643334556", "SSP", "BA", telefones, 22.5, SituacaoVendedorEnum.ATIVO, clientes1);
		Vendedor vendedor3 = criarVendedor(em, "22222222222", data, end, 
				"Vinicius", "8843212344", "SSP", "SP", telefones, 33.0, SituacaoVendedorEnum.SUSPENSO, clientes1);
		Vendedor vendedor4 = criarVendedor(em, "33333333333", data, end, 
				"Danilo", "3565672324", "SSP", "SP", telefones, 21.0, SituacaoVendedorEnum.ATIVO, clientes2);
		Vendedor vendedor5 = criarVendedor(em, "44444444444", data, end, 
				"Bruno", "1584202598", "SSP", "BA", telefones, 1.5, SituacaoVendedorEnum.SUSPENSO, clientes2);
		
		Administrativo adm1 = criarAdm(em, 1, "66666666666", data, end, 
				"Alan", "4487999987", "SSP", "SP", telefones);
		Administrativo adm2 = criarAdm(em, 3, "88888888888", data, end, 
				"Silvio", "6231432342", "SSP", "SP", telefones);
		Administrativo adm3 = criarAdm(em, 2, "77777777777", data, end, 
				"Santos", "2135532214", "SSP", "BA", telefones);
		
		em.getTransaction().commit();
		
	}

	private static Cidade criarCidade(EntityManager em, Estado estado, String sigla, String nome) {
		Cidade cidade = new Cidade();
		cidade.setSigla(sigla);
		cidade.setNome(nome);
		cidade.setEstado(estado);
		em.persist(cidade);
		return cidade;
	}

	private static Estado criarEstado(EntityManager em, String sigla, String nome) {
		Estado estado = new Estado();
		estado.setSigla(sigla);
		estado.setNome(nome);
		em.persist(estado);
		return estado;
	}

	private static RamoAtividade criarAtividade(EntityManager em, String nome) {
		RamoAtividade atividade = new RamoAtividade();
		atividade.setNome(nome);
		em.persist(atividade);
		return atividade;
	}

	private static PessoaJuridica criarPessoaJuridica(EntityManager em, String cnpj, String nome,
			ArrayList<RamoAtividade> ra_lista, double faturamento, ArrayList<Vendedor> vendedores) {
		PessoaJuridica pj = new PessoaJuridica();
		pj.setCnpj(cnpj);
		pj.setNome(nome);
		pj.setRamosAtividade(ra_lista);
		pj.setFaturamento(faturamento);
		pj.setVendedores(vendedores);
		em.persist(pj);
		return pj;
	}
	
	private static Vendedor criarVendedor(EntityManager em, String cpf, Date dataNascimento, Endereco endereo, String nome, String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones, Double percentualComissao, SituacaoVendedorEnum situacao, List<PessoaJuridica> clientes) {
		Vendedor vendedor = new Vendedor();
		vendedor.setCpf(cpf);
		vendedor.setDataNascimento(dataNascimento);
		vendedor.setEnderešo(endereo);
		vendedor.setNome(nome);
		vendedor.setRg(rg);
		vendedor.setRgOrgaoExpedidor(rgOrgaoExpedidor);
		vendedor.setRgUf(rgUf);
		vendedor.setTelefones(telefones);
		vendedor.setPercentualComissao(percentualComissao);
		vendedor.setSituacao(situacao);
		vendedor.setClientes(clientes);
		em.persist(vendedor);
		return vendedor;
	}

	private static Administrativo criarAdm(EntityManager em, int turno, String cpf, Date dataNascimento, Endereco endereco, String nome, String rg, String rgOrgaoExpedidor, String rgUf, ArrayList<String> telefones) {
		Administrativo adm = new Administrativo();
		adm.setCpf(cpf);
		adm.setDataNascimento(dataNascimento);
		adm.setEnderešo(endereco);
		adm.setNome(nome);
		adm.setRg(rg);
		adm.setRgOrgaoExpedidor(rgOrgaoExpedidor);
		adm.setRgUf(rgUf);
		adm.setTelefones(telefones);
		adm.setTurno(turno);
		em.persist(adm);
		return adm;
	}
	
	//Aqui ficam os selects
	
	//2 LETRA A
	private static void consultaClientesPorVendedor(EntityManager em, String nome_vendedor) {
		em.getTransaction().begin();
		Query query = em.createQuery("select pj.nome from PessoaJuridica pj join pj.vendedores v where v.nome=:nome_vendedor");
		query.setParameter("nome_vendedor", nome_vendedor);
		List<PessoaJuridica> pessoasJuridicas = query.getResultList();
		em.getTransaction().commit();
	}
	
	//2 LETRA B
	private static void consultaRamosAtividade(EntityManager em, String nome_ramo) {
		em.getTransaction().begin();
		Query query = em.createQuery("select v from Vendedor v join fetch v.telefones join v.clientes c join c.ramosAtividade r where r.nome=:nome_ramo");
		query.setParameter("nome_ramo", nome_ramo);
		List<Vendedor> vendedores = query.getResultList();
		em.getTransaction().commit();
	}
	
	//2 LETRA C
	private static void consultaFuncionariosAtivos(EntityManager em, SituacaoVendedorEnum situacao) {
		em.getTransaction().begin();
		Query query = em.createQuery("select f.cpf, f.nome from Funcionario f where f.situacao=:situacao");
		query.setParameter("situacao", situacao);
		List<Funcionario> funcionarios = query.getResultList();
		em.getTransaction().commit();
	}
	
	//2 LETRA D
	private static void consultaEstadoSemCidade(EntityManager em) {
		em.getTransaction().begin();
		Query queryEstados = em.createQuery("select e from Estado e left join e.cidades c where c is null");
		List<Estado> estados = queryEstados.getResultList();
		em.getTransaction().commit();
	}

	
	//2 LETRA E
	private static List<Object[]> consultaQuantidadeVendedoresCidade(EntityManager em) {
		return em.createQuery("select c, count(v) from Vendedor v join v.endereo.cidade c group by c").getResultList();
	}
	
	//2 LETRA F
	private static void consultaVendedoresDaCidade(EntityManager em, String nome_cidade) {
		em.getTransaction().begin();
		Query query = em.createQuery("select v from Vendedor v join fetch v.telefones join v.endereo.cidade c where c.nome=:nome_cidade");
		query.setParameter("nome_cidade", nome_cidade);
		List<Vendedor> vendedores = query.getResultList();
		em.getTransaction().commit();
	}
	

	
	

	

}