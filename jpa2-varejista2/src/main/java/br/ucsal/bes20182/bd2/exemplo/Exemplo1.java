package br.ucsal.bes20182.bd2.exemplo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.hibernate.query.NativeQuery;

import br.ucsal.bes20182.bd2.domain.Cidade;
import br.ucsal.bes20182.bd2.domain.Estado;

public class Exemplo1 {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("pu1");
			EntityManager em = emf.createEntityManager();

			popularBase(em);

			// O clear limpa o cache do entity manager, removendo todos os
			// objetos, inclusive os Estados.
			// Fizemos isso para ilustrar as consultas geradas pelo Lazy do
			// atributo cidades da classe Estado.
			// Nao devemos utilizar o clear no dia-a-dia.
			em.clear();

			// consultaCidadesDoEstado(em, "BA");

			// consultaCidadesDoEstadoNamedQuery(em, "BA");

			// consultaEstadoPorNomeCriteria(em, "Bahia");

			// Levanta a exception NoResultException
			// consultaEstadoPorNome(em, "Bahia1");

			// Levanta a exception NonUniqueResultException
			// consultaEstadoPorNome(em, "Bahia");

			atualizarNomeEstado(em, "BA", "Nova Bahia");

			// consultaEstados(em);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}

	private static void atualizarNomeEstado(EntityManager em, String sigla, String nome) {
		em.getTransaction().begin();
		@SuppressWarnings("unchecked")
		NativeQuery<Estado> query = (NativeQuery<Estado>) em.createNativeQuery("update tab_estado set nome=:nome where sigla=:sigla");
		query.setParameter("nome", nome);
		query.setParameter("sigla", sigla);
		query.executeUpdate();
		em.getTransaction().commit();
	}

	@SuppressWarnings("unused")
	private static void consultaEstadoPorNome(EntityManager em, String nome) {
		TypedQuery<Estado> query = em.createQuery("select e from Estado e where e.nome=:nome", Estado.class);
		query.setParameter("nome", nome);
		Estado estado = query.getSingleResult();
		System.out.println(estado);
	}

	@SuppressWarnings("unused")
	private static void consultaEstados(EntityManager em) {
		em.getTransaction().begin();
		TypedQuery<Estado> queryEstados = em.createQuery("select e from Estado e", Estado.class);
		List<Estado> estados = queryEstados.getResultList();
		for (Estado estado : estados) {
			System.out.println(estado);
		}
		em.getTransaction().commit();
	}

	@SuppressWarnings("unused")
	private static void consultaCidadesDoEstado(EntityManager em, String siglaEstado) {
		TypedQuery<Cidade> queryCidades = em.createQuery("select c from Cidade c where c.estado.sigla=:siglaEstado",
				Cidade.class);
		queryCidades.setParameter("siglaEstado", siglaEstado);
		List<Cidade> cidades = queryCidades.getResultList();
		for (Cidade cidade : cidades) {
			System.out.println(cidade);
		}
	}

	@SuppressWarnings("unused")
	private static void consultaCidadesDoEstadoNamedQuery(EntityManager em, String siglaEstado) {
		TypedQuery<Cidade> queryCidades = em.createNamedQuery("cidadesDoEstado", Cidade.class);
		queryCidades.setParameter("siglaEstado", siglaEstado);
		List<Cidade> cidades = queryCidades.getResultList();
		for (Cidade cidade : cidades) {
			System.out.println(cidade);
		}
	}

	private static void popularBase(EntityManager em) {
		em.getTransaction().begin();

		Estado estadoBA = criarEstado(em, "BA", "Bahia");
		Estado estadoPR = criarEstado(em, "PR", "Paran�");

		criarCidade(em, estadoBA, "SSA", "Salvador");
		criarCidade(em, estadoPR, "CJZ", "Cajazeiras X");
		criarCidade(em, estadoBA, "FDS", "Feira de Santana");

		Estado estadoBA2 = criarEstado(em, "B2", "Bahia");
		em.persist(estadoBA2);

		em.getTransaction().commit();
	}

	private static Cidade criarCidade(EntityManager em, Estado estado, String sigla, String nome) {
		Cidade cidade = new Cidade();
		cidade.setSigla(sigla);
		cidade.setNome(nome);
 		cidade.setEstado(estado);
		em.persist(cidade);
		return cidade;
	}

	private static Estado criarEstado(EntityManager em, String sigla, String nome) {
		Estado estado = new Estado();
		estado.setSigla(sigla);
		estado.setNome(nome);
		// em.persist(estado);
		return estado;
	}

}
